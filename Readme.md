
kubectl apply -f namespace.yml
kubectl apply -f secret.yml
kubectl apply -f configmap.yml
kubectl apply -f databases.yml
kubectl apply -f broker.yml
kubectl apply -f backend.yml
kubectl apply -f gateway.yml
kubectl apply -f frontend.yml
kubectl apply -f role.yml
kubectl apply -f rolebinding.yml
kubectl apply -f quota.yml
kubectl apply -f customer-budget.yml
kubectl apply -f frontend-budget.yml
kubectl apply -f gateway-budget.yml
kubectl apply -f vehicule-budget.yml
kubectl apply -f workshop-budget.yml
kubectl apply -f ingress.yml